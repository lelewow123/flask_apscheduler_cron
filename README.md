# flask_apscheduler任务调度系统
flask和apscheduler实现的任务调度系统

# front为前端代码，backend为后端代码



#安装说明
安装python3.X版本，然后运行pip install -r requirements.txt 进行python模块的安装



# nginx修改配置

location ^~ /api/{<br/>
	proxy_pass   http://127.0.0.1:5000/;<br/>
	proxy_redirect default;<br/>
	proxy_set_header HOST $host;<br/>
	proxy_set_header X-Real-IP $remote_addr;<br/>
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;<br/>
            
}<br/>

# 修改config.py配置<br/>
MYSQLHOST = "127.0.0.1";<br/>
MYSQLPORT = 3306;<br/>
MYSQLUSER = "root";<br/>
MYSQLPASS = "root";<br/>
MYSQLDB = "test";<br/>


# 后端运行<br/>
python3 run.py

