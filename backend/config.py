# -*- coding: utf-8 -*-
'''
by:lelewow@qq.com

'''
class Config(object):
    
    
    MYSQLHOST = "127.0.0.1";
    MYSQLPORT = 3306;
    MYSQLUSER = "root";
    MYSQLPASS = "root";
    MYSQLDB = "test";
    MYSQLTABLE = "jobs";
    MYSQLCHARSET='utf8mb4'
    MYSQLURL = "mysql+pymysql://%s:%s@%s/%s?charset=%s"%(MYSQLUSER,MYSQLPASS,MYSQLHOST,MYSQLDB,MYSQLCHARSET)
    LOGBASEPATH = "D:/workplace/APScheduler/";
    LOGPATH = LOGBASEPATH + "logs/";
    RUNPATH = LOGBASEPATH + "run/";
    LOGFORMAT = '%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s';
    SCHEDULER_API_ENABLED = True
    INITCRONTAB = '''
    CREATE  TABLE IF NOT EXISTS  `job_cron` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '任务id',
      `job_name` VARCHAR(255) NOT NULL COMMENT '任务名称',
      `job_desc` VARCHAR(255) NOT NULL COMMENT '任务介绍',
      `cmd_type` ENUM('php','python','bash','curl') NOT NULL DEFAULT 'bash' COMMENT '任务类型',
      `cmd` VARCHAR(100) NOT NULL COMMENT '任务命令',
      `process_only` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0:进程可多个 1：唯一进程',
      `trig` ENUM('interval','cron') NOT NULL COMMENT '任务种类',
      `job_time` CHAR(50) NOT NULL COMMENT '任务运行时间',
      `add_time` INT(11) NOT NULL DEFAULT '0' COMMENT '任务添加时间',
      `update_time` INT(11) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
      `stats` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0：正常 1：暂停 -1：删除',
      PRIMARY KEY (`id`),
      UNIQUE KEY `job_cmd` (`cmd_type`,`cmd`,`trig`)
    ) ENGINE=INNODB  DEFAULT CHARSET=utf8mb4;
    '''
    
    
    
    
    